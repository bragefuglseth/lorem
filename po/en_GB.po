# British English translation for lorem.
# Copyright (C) 2023 lorem's COPYRIGHT HOLDER
# This file is distributed under the same license as the lorem package.
# Bruce Cowan <bruce@bcowan.me.uk>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: lorem master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/lorem/issues\n"
"POT-Creation-Date: 2023-03-21 16:10+0000\n"
"PO-Revision-Date: 2023-03-22 15:21+0000\n"
"Last-Translator: Bruce Cowan <bruce@bcowan.me.uk>\n"
"Language-Team: English - United Kingdom <en@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 42.0\n"

#: data/org.gnome.design.Lorem.desktop.in.in:3
#: data/org.gnome.design.Lorem.metainfo.xml.in.in:7
msgid "Lorem"
msgstr "Lorem"

#: data/org.gnome.design.Lorem.desktop.in.in:4
#: data/org.gnome.design.Lorem.metainfo.xml.in.in:8 src/application.rs:109
msgid "Generate placeholder text"
msgstr "Generate placeholder text"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.design.Lorem.desktop.in.in:10
msgid "placeholder;"
msgstr "placeholder;"

#: data/org.gnome.design.Lorem.gschema.xml.in:10
#: data/org.gnome.design.Lorem.gschema.xml.in:11
msgid "Default window width"
msgstr "Default window width"

#: data/org.gnome.design.Lorem.gschema.xml.in:15
#: data/org.gnome.design.Lorem.gschema.xml.in:16
msgid "Default window height"
msgstr "Default window height"

#: data/org.gnome.design.Lorem.gschema.xml.in:20
msgid "Default window maximized behaviour"
msgstr "Default window maximised behaviour"

#: data/org.gnome.design.Lorem.metainfo.xml.in.in:10
msgid "Simple app to generate the well-known Lorem Ipsum placeholder text."
msgstr "Simple app to generate the well-known Lorem Ipsum placeholder text."

#: data/org.gnome.design.Lorem.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "Main Window"

#: data/org.gnome.design.Lorem.metainfo.xml.in.in:48
msgid "Maximiliano Sandoval"
msgstr "Maximiliano Sandoval"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "General"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Show Shortcuts"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Copy"
msgstr "Copy"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Main Menu"
msgstr "Main Menu"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "Quit"

#: data/resources/ui/window.ui:6
msgid "_Keyboard Shortcuts"
msgstr "_Keyboard Shortcuts"

#: data/resources/ui/window.ui:9
msgid "_About Lorem"
msgstr "_About Lorem"

#: data/resources/ui/window.ui:38
msgid "Amount"
msgstr "Amount"

#: data/resources/ui/window.ui:46
msgid "Type"
msgstr "Type"

#: data/resources/ui/window.ui:67
msgid "Main Menu"
msgstr "Main Menu"

#: data/resources/ui/window.ui:75
msgid "Copy"
msgstr "Copy"

#: src/application.rs:115
msgid "translator-credits"
msgstr "Bruce Cowan <bruce@bcowan.me.uk>"

#: src/window.rs:25
msgid "Paragraphs"
msgstr "Paragraphs"

#: src/window.rs:26
msgid "Words"
msgstr "Words"

#: src/window.rs:259
msgid "Selection copied"
msgstr "Selection copied"

#: src/window.rs:264
msgid "Copied"
msgstr "Copied"
